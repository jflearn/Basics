# -*- coding: utf-8 -*-
# @Time     : 2021/9/27 20:10
# @Author   : JustFly
# @File     : ThirdPartyModules.py
# @Software : PyCharm


import click

@click.command()
@click.password_option()
def passwd(password):
    click.echo('password: %s' % password)

if __name__ == '__main__':
    passwd()
