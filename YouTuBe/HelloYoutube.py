# -*- coding: utf-8 -*-
# @Time : 2021/6/18 21:33
# @Author : HUGBOY
# @File : HelloYoutube.py
# @Software: PyCharm


from tkinter import *

# 提交数据
def click(id, name, sex, age):

    print(id, name, sex, age)


def infoInput():
    # 主窗体
    mainWindow = Tk()
    mainWindow.geometry("1050x450")
    mainWindow.title('图形认知测试程序')
    icon = PhotoImage(file="image/hot.png")
    mainWindow.iconphoto(True, icon)
    mainWindow.config(background="SkyBlue")

    # 信息录入

    mainWindow.update()
    m_width = mainWindow.winfo_width()
    m_height = mainWindow.winfo_height()


    id_label = Label(mainWindow, text="身份证号:", font=('Comic Sans', 20), padx=m_width*0.21, pady=m_height*0.05, background="SkyBlue", anchor='c').grid(row=0)
    id_entry = Entry(mainWindow, font=('Comic Sans', 20))
    id_entry.grid(row=0, column=1)

    name_label = Label(mainWindow, text="姓名:", font=('Comic Sans', 20), padx=m_width*0.21, pady=m_height*0.05, bg="SkyBlue", anchor='c').grid(row=1)
    name_entry = Entry(mainWindow, font=('Comic Sans', 20))
    name_entry.grid(row=1, column=1)

    sex_label = Label(mainWindow, text="性别:", font=('Comic Sans', 20), padx=m_width*0.21, pady=m_height*0.05, bg="SkyBlue", anchor='c').grid(row=2)
    sex_select = IntVar()
    male_rb = Radiobutton(mainWindow, text='男', bg='SkyBlue', font=('Comic Sans', 20),  value=True, variable=sex_select).grid(row=2, column=1)
    female_rb = Radiobutton(mainWindow, text='女', bg='SkyBlue', font=('Comic Sans', 20),  value=False, variable=sex_select).grid(row=3, column=1)

    age_label = Label(mainWindow, text="年龄:", font=('Comic Sans', 20), padx=m_width*0.21, pady=m_height*0.05, bg="SkyBlue", anchor='c').grid(row=4)
    age_entry = Entry(mainWindow, font=('Comic Sans', 20))
    age_entry.grid(row=4, column=1)

    id = id_entry.get()
    name = name_entry.get()
    sex = sex_select.get()
    age = age_entry.get()

    saveButton = Button(mainWindow,
                    text='开始测试',
                    command=click(id, name, sex, age),
                    font=("Comic Sans", 20),
                    fg="black",
                    bg="white",
                    activeforeground="white",
                    activebackground="SkyBlue",
                    #state=DISABLED,
                    # image=icon,
                    #compound='bottom'
                        ).grid(row=5, column=2)



    mainWindow.mainloop()



if __name__ == '__main__':
    infoInput()






