#-*- coding: utf-8 -*-
#@Time : 2021/8/7 14:37
#@Author : HUGBOY
#@File : jj.py
#@Software: PyCharm

import tkinter as tk

index_num = 4


def up_page():
    info = frame_root.place_info()
    x = int(info['x']) + 100
    y = info['y']
    frame_root.place(x=x, y=y)
    global index_num
    index_num += 1
    index['text'] = "第 {} 页".format(index_num)


def down_page():
    info = frame_root.place_info()
    x = int(info['x']) - 100
    y = info['y']
    frame_root.place(x=x, y=y)
    global index_num
    index_num -= 1
    index['text'] = "第 {} 页".format(index_num)


root = tk.Tk()
root.title("滚动页面")
root.geometry("800x300+200+200")

#  显示框架- 大小和最小子框架相同
show_frame = tk.Frame(master=root, width=100, height=200, bg="black")
show_frame.place(x=10, y=10)
#  测试会不会多放一个
show_frame.place(x=250, y=10)

#  容器框架，大小是所有子框架的大小总和--放在显示框架里
frame_root = tk.Frame(master=show_frame, width=300, height=200, bg="green")
frame_root.place(x=0, y=0)

# 最小子框架---放在容器框架里
frame_1 = tk.Frame(master=frame_root, width=100, height=200, bg="cyan")
frame_1.place(x=0, y=10)
tk.Label(master=frame_1, text="第四页").grid(row=0, column=0)
tk.Entry(master=frame_1).grid(row=0, column=1)

frame_2 = tk.Frame(master=frame_root, width=100, height=200, bg="blue")
frame_2.place(x=100, y=10)
tk.Label(master=frame_2, text="第三页").grid(row=0, column=0)
tk.Entry(master=frame_2).grid(row=0, column=1)

frame_3 = tk.Frame(master=frame_root, width=100, height=200, bg="yellow")
frame_3.place(x=200, y=10)
tk.Label(master=frame_3, text="第二页").grid(row=0, column=0)
tk.Entry(master=frame_3).grid(row=0, column=1)

index = tk.Label(text=" 第 4 页 ")
index.pack()

tk.Button(text="上一页", command=up_page).pack()
tk.Button(text="下一页", command=down_page).pack()

root.mainloop()