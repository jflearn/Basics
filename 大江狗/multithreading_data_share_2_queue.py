# -*- coding: utf-8 -*-
# @Time     : 2022/6/19 18:30
# @Author   : JustFly
# @File     : multithreading_data_share_2_queue.py
# @Software : PyCharm

from queue import Queue
import random, threading, time


# producer class
class Producer(threading.Thread):
    def __init__(self, name, queue):
        threading.Thread.__init__(self, name=name)
        self.queue = queue

    def run(self):
        for i in range(1, 5):
            print(f'{self.getName()} 生产 iPhone {i} 放到 queue !')
            self.queue.put(i)
            time.sleep(random.randrange(10) / 5)

        print(f'{self.getName()} 完成 ！')

# consumer class
class Consumer(threading.Thread):
    def __init__(self, name, queue):
        threading.Thread.__init__(self, name=name)
        self.queue = queue

    def run(self):
        for i in range(1, 5):
            val = self.queue.get()
            print(f'{self.getName()} 消费 iPhone {val} 从 queue !')
            time.sleep(random.randrange(10))

        print(f'{self.getName() }完成 ！')

def main():
    queue = Queue()
    p = Producer('PRODUCER', queue)
    c = Consumer('CONSUMER', queue)

    p.start()
    c.start()

    p.join()
    c.join()

    print('All threads finished !')

if __name__ == '__main__':
    main()