# -*- coding: utf-8 -*-
# @Time     : 2022/6/2 22:43
# @Author   : JustFly
# @File     : multiprocess_pool.py
# @Software : PyCharm

from multiprocessing import Pool, cpu_count
import os
import time


def long_time_task(i):
    print(f'子进程-{i+1}：{os.getpid()}')
    time.sleep(2)
    print(f'结果：{8 ** 20}')


if __name__ == '__main__':
    print(f"CPU内核数：{cpu_count()}")
    print(f'当前母进程: {os.getpid()}')
    start = time.time()

    p = Pool(4)

    for i in range(4):
        # 向进程池 提交 需要执行的函数及参数
        p.apply_async(long_time_task, args=(i, ))

    print("等待所有子进程完成...")
    p.close()  # 不再接受新的进程
    p.join()

    end = time.time()

    print(f"总用时{end - start}秒")
