# -*- coding: utf-8 -*-
# @Time     : 2022/6/2 22:04
# @Author   : JustFly
# @File     : process.py
# @Software : PyCharm
"""
程序              进程                  线程
含1或多个进程      含1个或多个线程
                  独立内存单元           共享进程的内存单元

"""

import time
import os


def long_time_task():
    print(f'当前进程: {os.getpid()}')
    time.sleep(2)
    print(f"结果: {8 ** 20}")


if __name__ == "__main__":
    print(f'当前母进程：{os.getpid()}')
    start = time.time()
    for i in range(2):
        long_time_task()

    end = time.time()
    print(f"用时{end-start}秒")
