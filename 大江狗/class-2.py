# -*- coding: utf-8 -*-
# @Time     : 2022/5/29 22:21
# @Author   : JustFly
# @File     : class-2.py
# @Software : PyCharm

# 父类
class SchoolMember:

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def tell(self):
        print(f"Name: {self.name}, Age: {self.age}")

    def sayHello(self):
        print("Hello ! SchoolMember!")



# 子类
class Teacher(SchoolMember):

    def __init__(self, name, age, salary):
        SchoolMember.__init__(self, name, age)  # 继承父类 Initialize...
        self.salary = salary

    def tell(self):  # 重写方法
        SchoolMember.tell(self)
        print(f"Salary: {self.salary}")


# 子类
class Student(SchoolMember):

    def __init__(self, name, age, score):
        super().__init__(name, age)  # super() 等价于 SchoolMember.__init__(self)
        self.score = score

    def tell(self):
        SchoolMember.tell(self)
        print(f"Score: {self.score}")

    @staticmethod  # 静态方法   无法使用cls和self参数访问类或实例的变量 不含cls self 参数
    def fun1():
        print("just a static.")


stu1 = Student("贾建飞", "21", "80")
tch1 = Teacher("李老师", "45", "$1000")

stu1.tell()
tch1.tell()

stu1.sayHello()
tch1.sayHello()

stu1.fun1()