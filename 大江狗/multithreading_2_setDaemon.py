# -*- coding: utf-8 -*-
# @Time     : 2022/6/19 17:39
# @Author   : JustFly
# @File     : multithreading_2_setDaemon.py
# @Software : PyCharm

#  实现主线程结束后不再执行子线程

import threading
import time

def long_time_task():
    print(f'当前子线程：{threading.current_thread().name}')
    time.sleep(2)
    print(f'结果：{8 ** 20}')

if __name__ == '__main__':
    start = time.time()
    print(f'主线程：{threading.current_thread().name}')
    for i in range(5):
        t = threading.Thread(target=long_time_task, args=())
        t.setDaemon(True)
        t.start()

    end = time.time()
    print(f'总用时{end - start}秒')

