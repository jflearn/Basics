# -*- coding: utf-8 -*-
# @Time     : 2022/5/29 21:45
# @Author   : JustFly
# @File     : class.py
# @Software : PyCharm

class Student:
    count = 0  # class variables

    def __init__(self, name, score):
        self.name = name
        self.__score = score  # private attribute 外部不能直接访问

        self.__class__.count += 1  # 统计对象个数


    def show(self):
        print(f"Name:{self.name} Score:{self.__score}")  # 内部函数可访问私有属性

        self.__print()

    def __print(self):  # 私有方法 外部不能直接调用
        print(f"Hello...{self.name}")

    @classmethod  # 说明此处定义的是类方法
    def total(cls):
        print(f"Total: {cls.count}")

    @property  # 把函数伪装成属性
    def sscore(self):
        print(f"score: {self.__score}")


stu1 = Student("贾建飞", "650")
stu2 = Student("张居正", "900")
stu3 = Student("吴亚超", "660")

stu1.show()
stu2.show()
stu3.show()

print(stu1.__class__.count)
print(Student.count)

stu1.__class__.total()
Student.total()

# print(stu1.__score) # AttributeError: 'Student' object has no attribute '__score'
# stu1.__print() # AttributeError: 'Student' object has no attribute '__print'

stu1.sscore
