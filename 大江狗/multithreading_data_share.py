# -*- coding: utf-8 -*-
# @Time     : 2022/6/19 18:14
# @Author   : JustFly
# @File     : multithreading_data_share.py
# @Software : PyCharm

# lock

import threading

class Account:
    def __init__(self):
        self.balance = 0

    def add(self, lock):
        # 获得锁
        lock.acquire()

        for i in range(0, 10):
            self.balance += 1
            print(f'func: ADD balance: {self.balance}')

        # 释放锁
        lock.release()

    def delete(self, lock):
        lock.acquire()

        for i in range(0, 10):
            self.balance -= 1
            print(f'func: DELETE balance: {self.balance}')

        lock.release()

if __name__ == '__main__':
    account = Account()
    lock = threading.Lock()

    # create thread
    thread_add = threading.Thread(target=account.add, args=(lock, ), name='Add')
    thread_delete = threading.Thread(target=account.delete, args=(lock, ), name='Delete')

    # start thread
    thread_add.start()
    thread_delete.start()

    # waite thread
    thread_add.join()
    thread_delete.join()

    print(f'The final balance is: {account.balance}')



