# -*- coding: utf-8 -*-
# @Time     : 2022/6/11 18:01
# @Author   : JustFly
# @File     : multithreading.py
# @Software : PyCharm


# 通过类创建新线程

import threading
import time

def long_time_task(i):
    print(f'当前子线程：{threading.current_thread().name} 任务{i}')
    time.sleep(2)
    print(f'结果：{8 ** 20}')


if __name__ == '__main__':
    start = time.time()

    print(f"主线程：{threading.current_thread().name}")
    t1 = threading.Thread(target=long_time_task, args=(1, ))
    t2 = threading.Thread(target=long_time_task, args=(2, ))

    t1.start()
    t2.start()

    t1.join()
    t2.join()

    end = time.time()

    print(f'总用时:{end - start} s')