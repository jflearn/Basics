# -*- coding: utf-8 -*-
# @Time     : 2022/6/2 22:25
# @Author   : JustFly
# @File     : multiprocess.py
# @Software : PyCharm

import os
import time

from multiprocessing import Process

def long_time_task(i):
    print(f'子进程：{os.getpid()} 任务编号： {i}')
    time.sleep(2)
    print(f"结果：{8 ** 20}")


if __name__ == '__main__':
    print(f"当前母进程：{os.getpid()}\n")
    start = time.time()

    p1 = Process(target=long_time_task, args=(1, ))
    p2 = Process(target=long_time_task, args=(2, ))
    print("等待所有子进程完成...")
    p1.start()
    p2.start()
    p1.join()  # 让母进程阻塞 等待子进程执行结束 再继续下一步
    p2.join()

    end = time.time()
    print(f"\n总用时{end - start}秒")

"""
D:\python39\python.exe E:/PYTHON/Basics/大江狗/multiprocess.py
当前母进程：20752

等待所有子进程完成...
子进程：17464 任务编号： 1
子进程：6480 任务编号： 2
结果：1152921504606846976
结果：1152921504606846976

总用时2.428980588912964秒

进程已结束，退出代码为 0

"""