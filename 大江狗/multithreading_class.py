# -*- coding: utf-8 -*-
# @Time     : 2022/6/19 17:52
# @Author   : JustFly
# @File     : multithreading_class.py
# @Software : PyCharm
import threading
import time
import threading


def long_time_task(i):
    time.sleep(2)
    return 8 ** 20


class MyThread(threading.Thread):
    def __init__(self, func, args, name='', ):
        threading.Thread.__init__(self)
        self.func = func
        self.args = args
        self.name = name
        self.result = None

    def run(self):
        print(f'开始子进程：{self.name}')
        self.result = self.func(self.args[0], )
        print(f'结果：{self.result}')
        print(f'结束子进程：{self.name}')


if __name__ == '__main__':
    start = time.time()
    threads = []
    for i in range(1, 3):
        t = MyThread(long_time_task, (i,), str(i))
        threads.append(t)

    for t in threads:
        t.start()
    for t in threads:
        t.join()

    end = time.time()
    print(f'总共用时{end - start}秒....')
